from django.apps import AppConfig


class ElecmaxppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'elecMaxpp'
