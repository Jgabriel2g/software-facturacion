from django.conf                                      import settings
from django.db.models.query import QuerySet
from django.http import request
from rest_framework                                   import generics, status
from rest_framework.response                          import Response
from rest_framework.permissions                       import IsAuthenticated
from rest_framework_simplejwt.backends                import TokenBackend

from elecMaxpp.models.user import User
from elecMaxpp.models.factura import Facturas
from elecMaxpp.serializers.facturasSerializers import FacturaSerializer

class FacturasCreateView(generics.CreateAPIView):
    queryset = Facturas.objects.all()
    serializer_class = FacturaSerializer
    permission_classes = (IsAuthenticated,)
    def psot(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = TokenBackend.decode(token, verify=False)