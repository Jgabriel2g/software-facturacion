from django.db import models

class User(models.Model): 
    id       = models.BigAutoField(primary_key=True)
    username = models.CharField('username',max_length=45)
    password=models.CharField('Password',max_length=20)
    nombre = models.CharField('nombre', max_length=45)
    email = models.EmailField('Email', max_length=100, unique=True)
