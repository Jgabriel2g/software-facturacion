from django.db import models
from .user import User

class Facturas(models.Model):
    idFactura = models.AutoField('IdFactura',primary_key=True)
    vendedor = models.CharField('Vendedor',max_length=45)
    idCliente = models.ForeignKey(User, related_name='Cliente',on_delete=models.CASCADE)
    cliente = models.CharField('Cliente', max_length=45)
    email = models.EmailField('Email', max_length=100, unique=True)
    direccion = models.CharField('Direccion',max_length=45)
    producto = models.CharField('Producto',max_length=45)
    cantidad = models.IntegerField('Cantidad',default=0)
    valor = models.IntegerField('Valor',default=0)
    impuesto = models.IntegerField('Impuesto',default=0)
    total = models.IntegerField('Total',default=0)

