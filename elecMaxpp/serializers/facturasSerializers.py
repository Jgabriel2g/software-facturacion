from elecMaxpp.models.factura import Facturas
from elecMaxpp.models.user import User
from rest_framework import serializers

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facturas
        fields = ["cliente", "vendedor", "email", "direccion","producto", "cantidad ", "valor", "impuesto", "total"]
    
    def to_representation(self, obj):
        factura = Facturas.objects.get(id=obj.id)
        user = User.objects.get(id=obj.id)
        return{
            'id': factura.id,
            'vendedor': factura.vendedor,
            'director': factura.direccion,
            'cantidad': factura.cantidad,
            'producto': factura.producto,
            'total': factura.total,
            'user': {
                'id': user.id,
                'cliente': user.cliente,
            }

        }