from elecMaxpp.models.user import User
from elecMaxpp.models.factura import Facturas
from rest_framework import serializers

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facturas
        fields = ["id", "nombre", "username", "password", "email"]
    
    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id'      : user.id,
            'username': user.username,
            'name'    : user.name,
            'email'   : user.email,
        }